/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.chungkwong.json;
import java.util.*;
import java.util.stream.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class JSONArray implements JSONStuff{
	private final List<JSONStuff> elements;
	public JSONArray(List<JSONStuff> elements){
		this.elements=elements;
	}
	@Override
	public String toString(){
		return elements.stream().map((o)->o.toString()).collect(Collectors.joining(",","[","]"));
	}
	@Override
	public boolean equals(Object obj){
		return obj instanceof JSONArray&&((JSONArray)obj).elements.equals(elements);
	}
	@Override
	public int hashCode(){
		int hash=3;
		hash=17*hash+Objects.hashCode(this.elements);
		return hash;
	}
	public List<JSONStuff> getElements(){
		return elements;
	}
}
