/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.chungkwong.json;
import java.util.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class JSONNumber implements JSONStuff{
	private final Number value;
	public JSONNumber(Number value){
		this.value=value;
	}
	@Override
	public String toString(){
		return Objects.toString(value);
	}
	@Override
	public boolean equals(Object obj){
		return obj instanceof JSONNumber&&((JSONNumber)obj).value.doubleValue()==value.doubleValue();
	}
	@Override
	public int hashCode(){
		int hash=3;
		hash=73*hash+Objects.hashCode(this.value.doubleValue());
		return hash;
	}
	public Number getValue(){
		return value;
	}
}
