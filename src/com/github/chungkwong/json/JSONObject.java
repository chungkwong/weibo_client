/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.chungkwong.json;
import java.util.*;
import java.util.stream.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class JSONObject implements JSONStuff{
	private final Map<JSONStuff,JSONStuff> members;
	public JSONObject(Map<JSONStuff,JSONStuff> members){
		this.members=members;
	}
	@Override
	public String toString(){
		return members.entrySet().stream().map((o)->o.getKey()+":"+o.getValue()).collect(Collectors.joining(",","{","}"));
	}
	@Override
	public boolean equals(Object obj){
		return obj instanceof JSONObject&&((JSONObject)obj).members.equals(members);
	}
	@Override
	public int hashCode(){
		int hash=3;
		hash=19*hash+Objects.hashCode(this.members);
		return hash;
	}
	public Map<JSONStuff,JSONStuff> getMembers(){
		return members;
	}
}
