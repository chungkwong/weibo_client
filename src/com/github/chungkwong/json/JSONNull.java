/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.chungkwong.json;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class JSONNull implements JSONStuff{
	public static final JSONNull INSTANCE=new JSONNull();
	private JSONNull(){
	}
	@Override
	public String toString(){
		return "null";
	}
	@Override
	public boolean equals(Object obj){
		return INSTANCE==obj;
	}
	@Override
	public int hashCode(){
		int hash=7;
		return hash;
	}
}
