/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.chungkwong.json;
import java.util.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class JSONString implements JSONStuff{
	private final String value;
	public JSONString(String value){
		this.value=value;
	}
	@Override
	public String toString(){
		return "\""+value+"\"";
	}
	@Override
	public boolean equals(Object obj){
		return obj instanceof JSONString&&((JSONString)obj).value.equals(value);
	}
	@Override
	public int hashCode(){
		int hash=7;
		hash=67*hash+Objects.hashCode(this.value);
		return hash;
	}
	public String getValue(){
		return value;
	}
}