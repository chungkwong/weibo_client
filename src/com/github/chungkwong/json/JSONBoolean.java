/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.github.chungkwong.json;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class JSONBoolean implements JSONStuff{
	public static JSONBoolean TRUE=new JSONBoolean(true);
	public static JSONBoolean FALSE=new JSONBoolean(false);
	private final boolean value;
	private JSONBoolean(boolean value){
		this.value=value;
	}
	@Override
	public String toString(){
		return Boolean.toString(value);
	}
	@Override
	public boolean equals(Object obj){
		return obj instanceof JSONBoolean&&((JSONBoolean)obj).value==value;
	}
	@Override
	public int hashCode(){
		int hash=7;
		hash=31*hash+(this.value?1:0);
		return hash;
	}
	public boolean getValue(){
		return value;
	}
}
