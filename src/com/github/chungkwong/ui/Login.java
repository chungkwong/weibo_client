/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.ui;
import com.github.chungkwong.json.*;
import com.github.chungkwong.net.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.util.prefs.*;
import javax.swing.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Login extends JPanel implements ActionListener{
	private static final String clientId="";//TODO: 请填真实信息
	private static final String clientSecret="";//TODO: 请填真实信息
	private static final String redirectUri="";//TODO: 请填真实信息
	private final JTextField accessToken=new JTextField();
	public Login(){
		setLayout(new BorderLayout());
		add(new JLabel("access token:"),BorderLayout.WEST);
		accessToken.setText(Preferences.userNodeForPackage(Login.class).get("access_token",""));
		accessToken.addActionListener((e)->cacheToken());
		add(accessToken,BorderLayout.CENTER);
		JButton get=new JButton("首次用本程序请点此获取access token");
		get.addActionListener(this);
		add(get,BorderLayout.EAST);
	}
	@Override
	public void actionPerformed(ActionEvent e){
		try{
			Desktop.getDesktop().browse(new URI("https://api.weibo.com/oauth2/authorize?client_id="+clientId+"&response_type=code&redirect_uri="+redirectUri+"&forcelogin=true"));
			String code=JOptionPane.showInputDialog("请输入授权成功后的页面地址栏中的参数code");
			HttpPostRequest request=new HttpPostRequest();
			request.addField("client_id",clientId);
			request.addField("client_secret",clientSecret);
			request.addField("grant_type","authorization_code");
			request.addField("redirect_uri",redirectUri);
			request.addField("code",code);
			JSONObject ret=HttpClient.requestJSON("https://api.weibo.com/oauth2/access_token",request);
			String token=((JSONString)ret.getMembers().get(new JSONString("access_token"))).getValue();
			accessToken.setText(token);
			cacheToken();
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null,"认证失败，请重试");
			ex.printStackTrace();
		}
	}
	private void cacheToken(){
		Preferences pref=Preferences.userNodeForPackage(Login.class);
		pref.put("access_token",accessToken.getText());
		try{
			pref.flush();
		}catch(BackingStoreException ex){
			ex.printStackTrace();
		}
	}
}
