/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.ui;
import com.github.chungkwong.json.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class SubjectViewer extends JPanel{
	public SubjectViewer(JSONStuff json){
		setLayout(new BorderLayout());
		JTree tree=new JTree(new JSONTreeModel(json));
		add(tree,BorderLayout.CENTER);
	}
	static class JSONTreeModel implements TreeModel{
		private final JSONStuff root;
		public JSONTreeModel(JSONStuff root){
			this.root=root;
		}
		@Override
		public Object getRoot(){
			return root;
		}
		@Override
		public Object getChild(Object parent,int index){
			if(parent instanceof JSONObject){
				if(index%2==0){
					return ((JSONObject)parent).getMembers().keySet().toArray()[index/2];
				}else{
					return ((JSONObject)parent).getMembers().values().toArray()[index/2];
				}
			}else if(parent instanceof JSONArray){
				return ((JSONArray)parent).getElements().get(index);
			}else{
				return null;
			}
		}
		@Override
		public int getChildCount(Object parent){
			if(parent instanceof JSONObject){
				return ((JSONObject)parent).getMembers().size()*2;
			}else if(parent instanceof JSONArray){
				return ((JSONArray)parent).getElements().size();
			}else{
				return 0;
			}
		}
		@Override
		public boolean isLeaf(Object node){
			return !(node instanceof JSONObject||node instanceof JSONArray);
		}
		@Override
		public void valueForPathChanged(TreePath path,Object newValue){
		}
		@Override
		public int getIndexOfChild(Object parent,Object child){
			int length=getChildCount(parent);
			for(int i=0;i<length;i++){
				if(getChild(parent,i)==child){
					return i;
				}
			}
			return -1;
		}
		@Override
		public void addTreeModelListener(TreeModelListener l){
		}
		@Override
		public void removeTreeModelListener(TreeModelListener l){
		}
	}
}
