/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.ui;
import com.github.chungkwong.json.*;
import com.github.chungkwong.net.*;
import com.github.chungkwong.weibo.action.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class ActionEditor extends JPanel implements ActionListener{
	private final JCheckBox[] set;
	private final JTextField[] content;
	private final Box output;
	private final WeiboAction action;
	public ActionEditor(WeiboAction action){
		this.action=action;
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		JComponent panel=new JPanel();
		GroupLayout layout=new GroupLayout(panel);
		panel.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		GroupLayout.SequentialGroup hGroup=layout.createSequentialGroup();
		GroupLayout.SequentialGroup vGroup=layout.createSequentialGroup();
		GroupLayout.ParallelGroup col0=layout.createParallelGroup();
		GroupLayout.ParallelGroup col1=layout.createParallelGroup();
		GroupLayout.ParallelGroup col2=layout.createParallelGroup();
		Parameter[] parameters=action.getParameters();
		set=new JCheckBox[parameters.length];
		content=new JTextField[parameters.length];
		for(int i=0;i<parameters.length;i++){
			set[i]=new JCheckBox();
			if(parameters[i].isNecesssary()){
				set[i].setSelected(true);
				set[i].setEnabled(false);
			}
			JLabel key=new JLabel(parameters[i].getIdentifier());
			content[i]=new JTextField();
			content[i].setToolTipText(parameters[i].getDescription());
			col0.addComponent(set[i]);
			col1.addComponent(key);
			col2.addComponent(content[i]);
			vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(set[i]).addComponent(key).addComponent(content[i]));
		}
		hGroup.addGroup(col0);
		hGroup.addGroup(col1);
		hGroup.addGroup(col2);
		layout.setHorizontalGroup(hGroup);
		layout.setVerticalGroup(vGroup);
		add(panel);
		JButton submit=new JButton("提交");
		submit.addActionListener(this);
		add(submit);
		output=Box.createVerticalBox();
		add(new JScrollPane(output));
	}
	@Override
	public void actionPerformed(ActionEvent e){
		try{
			HttpRequest request=HttpClient.createRequest(action.isPost(),action.isMultiPart());
			Parameter[] parameters=action.getParameters();
			for(int i=0;i<set.length;i++){
				if(set[i].isSelected()){
					if(parameters[i].getRange()==Range.BINARY){
						request.addFile(new File(content[i].getText()));
					}else{
						request.addField(parameters[i].getIdentifier(),content[i].getText());
					}
				}
			}
			JSONObject result=HttpClient.requestJSON(action.getURL(),request);
			if(!isError(result)){
				output.add(action.getReturnSchema().createViewer(result));
			}
		}catch(Exception ex){
			JOptionPane.showConfirmDialog(null,"提交失败"+ex.toString());
			Logger.getGlobal().log(Level.SEVERE,"",ex);
		}
		validate();
	}
	private static final JSONString ERROR_CODE=new JSONString("error_code");
	private static boolean isError(JSONStuff json){
		if(json instanceof JSONObject){
			Map<JSONStuff,JSONStuff> members=((JSONObject)json).getMembers();
			if(members.containsKey(ERROR_CODE)){
				String code=((JSONString)members.get(ERROR_CODE)).getValue();
				ResourceBundle bundle=ResourceBundle.getBundle("com/github/chungkwong/ui/ERROR_CODE");
				if(bundle.containsKey(code)){
					JOptionPane.showConfirmDialog(null,"被服务器拒绝:"+bundle.getString(code));
					return true;
				}
			}
		}
		return false;
	}
}
