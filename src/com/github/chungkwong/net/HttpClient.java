/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.net;
import com.github.chungkwong.json.*;
import java.io.*;
import java.security.cert.*;
import javax.net.ssl.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class HttpClient{
	static{
		try{
			trustAllHttpsCertificates();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	public static HttpRequest createRequest(boolean post,boolean multipart){
		if(post)
			return multipart?new HttpMultipartRequest():new HttpPostRequest();
		else
			return new HttpGetRequest();
	}
	public static JSONObject requestJSON(String url,HttpRequest request)throws Exception{
		return (JSONObject)JSONParser.parse(new InputStreamReader(request.send(url),"UTF-8"));
	}
	private static void trustAllHttpsCertificates() throws Exception{
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		trustAllCerts[0] = new X509TrustManager(){
			@Override
			public X509Certificate[] getAcceptedIssuers(){return null;}
			@Override
			public void checkServerTrusted(X509Certificate[] arg0, String arg1)throws CertificateException{}
			@Override
			public void checkClientTrusted(X509Certificate[] arg0, String arg1)throws CertificateException{}
		};
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}
}
