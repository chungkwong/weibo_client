/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.net;
import java.io.*;
import java.net.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class HttpMultipartRequest implements HttpRequest{
	private static final String BOUNDARY="--"+"aifudao7816510d1hq"+"\r\n";
	private final ByteArrayOutputStream buf=new ByteArrayOutputStream();
	@Override
	public void addField(String key,String value) throws Exception{
		StringBuilder sb = new StringBuilder();
		sb.append(BOUNDARY);
		sb.append("Content-Disposition:form-data;name=\"");
		sb.append(key);
		sb.append("\"\r\n\r\n");
		sb.append(URLEncoder.encode(value,"UTF-8"));
		sb.append("\r\n");
		buf.write(sb.toString().getBytes());
	}
	@Override
	public void addFile(File file) throws Exception{
		StringBuilder sb = new StringBuilder();
		sb.append(BOUNDARY);
		sb.append("Content-Disposition:form-data;name=\"pic\";filename=\"");
		sb.append(URLEncoder.encode(file.getName(),"UTF-8"));
		sb.append("\"\r\nContent-type: image/png\r\nContent-Transfer-Encoding: binary\r\n\r\n");
		buf.write(sb.toString().getBytes());
		try (FileInputStream fis=new FileInputStream(file)) {
			byte[] buffer = new byte[8192]; // 8k
			int count;
			while ((count = fis.read(buffer)) != -1) {
				buf.write(buffer, 0, count);
			}
			buf.write("\r\n\r\n".getBytes());
		}
		buf.write(("--" + "aifudao7816510d1hq" + "--\r\n").getBytes());
	}
	@Override
	public void end(){
	}
	@Override
	public InputStream send(String url) throws Exception{
		URLConnection conn = new URL(url).openConnection();
		conn.setDoOutput(true);
		conn.setRequestProperty("Content-type","multipart/form-data;boundary="+"aifudao7816510d1hq");
		conn.connect();
		try (BufferedOutputStream out=new BufferedOutputStream(conn.getOutputStream())) {
			out.write(buf.toByteArray());
			out.flush();
		}
		return conn.getInputStream();
	}

}
