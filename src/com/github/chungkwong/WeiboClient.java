/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong;
import com.github.chungkwong.ui.*;
import com.github.chungkwong.weibo.action.*;
import java.awt.*;
import javax.swing.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class WeiboClient extends JPanel{
	private final Login account=new Login();
	private static final WeiboAction[] actions=new WeiboAction[]{
		Share.INSTANCE,Go.INSTANCE,Mentions.INSTANCE,Show.INSTANCE,Count.INSTANCE,
		HomeTimeline.INSTANCE,UserTimeline.INSTANCE,RepostTimeline.INSTANCE,
		CountUser.INSTANCE,ShowUser.INSTANCE,Friend.INSTANCE,Follower.INSTANCE
	};
	public WeiboClient(){
		setLayout(new BorderLayout());
		add(account,BorderLayout.NORTH);
		//add(new ActionEditor(Update.INSTANCE),BorderLayout.CENTER);
		JTabbedPane tab=new JTabbedPane(JTabbedPane.LEFT);
		for(WeiboAction action:actions){
			tab.addTab(action.getName(),new ActionEditor(action));
		}
		add(tab,BorderLayout.CENTER);
	}
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args){
		JFrame f=new JFrame("史上最难用微博客户端");
		f.add(new WeiboClient(),BorderLayout.CENTER);
		f.setSize(800,600);
		f.setExtendedState(JFrame.MAXIMIZED_BOTH);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
