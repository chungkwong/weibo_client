/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Comment extends Subject{
	public static Comment INSTANCE=new Comment();
	private Comment(){
		addField(new Field("created_at",Schemas.STRING,"评论创建时间",true));
		addField(new Field("id",Schemas.LONG,"评论的ID"));
		addField(new Field("text",Schemas.STRING,"评论的内容",true));
		addField(new Field("source",Schemas.STRING,"评论的来源"));
		addField(new Field("user",User.INSTANCE,"评论作者的用户信息字段",true));
		addField(new Field("mid",Schemas.STRING,"评论的MID"));
		addField(new Field("idstr",Schemas.STRING,"字符串型的评论ID"));
		addField(new Field("status",Comment.INSTANCE,"评论的微博信息字段"));
		addField(new Field("reply_comment",Comment.INSTANCE,"评论来源评论"));
	}
	@Override
	public String getName(){
		return "评论";
	}
}
