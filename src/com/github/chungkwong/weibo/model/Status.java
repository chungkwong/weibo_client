/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Status extends Subject{
	public static Status INSTANCE=new Status();
	private Status(){
		addField(new Field("created_at",Schemas.STRING,"微博创建时间",true));
		addField(new Field("id",Schemas.LONG,"微博ID"));
		addField(new Field("mid",Schemas.LONG,"微博MID"));
		addField(new Field("idstr",Schemas.STRING,"字符串型的微博ID"));
		addField(new Field("text",Schemas.STRING,"微博信息内容",true));
		addField(new Field("source",Schemas.STRING,"微博来源"));
		addField(new Field("favorited",Schemas.BOOLEAN,"是否已收藏"));
		addField(new Field("truncated",Schemas.BOOLEAN,"是否被截断"));
		addField(new Field("in_reply_to_status_id",Schemas.STRING,"（暂未支持）回复ID"));
		addField(new Field("in_reply_to_user_id",Schemas.STRING,"（暂未支持）回复人UID"));
		addField(new Field("in_reply_to_screen_name",Schemas.STRING,"（暂未支持）回复人昵称"));
		addField(new Field("thumbnail_pic",Schemas.STRING,"缩略图片地址"));
		addField(new Field("bmiddle_pic",Schemas.STRING,"中等尺寸图片地址"));
		addField(new Field("original_pic",Schemas.STRING,"原始图片地址"));
		addField(new Field("geo",Location.INSTANCE,"地理信息字段"));
		addField(new Field("user",User.INSTANCE,"微博作者的用户信息字段",true));
		addField(new Field("retweeted_status",Status.INSTANCE,"被转发的原微博信息字段"));
		addField(new Field("reposts_count",Schemas.INT,"转发数"));
		addField(new Field("comments_count",Schemas.INT,"评论数"));
		addField(new Field("attitudes_count",Schemas.INT,"表态数"));
		addField(new Field("mlevel",Schemas.INT,"暂未支持"));
		addField(new Field("visible",Schemas.UNKNOWN,"微博的可见性及指定可见分组信息。","该object中type取值，0：普通微博，1：私密微博，3：指定分组微博，4：密友微博；list_id为分组的组号"));
		addField(new Field("pic_ids",Schemas.UNKNOWN,"微博配图ID。","多图时返回多图ID，用来拼接图片url。用返回字段thumbnail_pic的地址配上该返回字段的图片ID，即可得到多个图片url。"));
		addField(new Field("ad",Schemas.toArraySchema(Schemas.LONG),"微博流内的推广微博ID"));
	}
	@Override
	public String getName(){
		return "微博";
	}

}
