/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Privacy extends Subject{
	public static Privacy INSTANCE=new Privacy();
	private Privacy(){
		addField(new Field("comment",Schemas.INT,"是否可以评论我的微博","0：所有人、1：关注的人、2：可信用户"));
		addField(new Field("geo",Schemas.INT,"是否开启地理信息","0：不开启、1：开启"));
		addField(new Field("message",Schemas.INT,"是否可以给我发私信","0：所有人、1：我关注的人、2：可信用户"));
		addField(new Field("realname",Schemas.INT,"是否可以通过真名搜索到我","0：不可以、1：可以"));
		addField(new Field("badge",Schemas.INT,"勋章是否可见","0：不可见、1：可见"));
		addField(new Field("mobile",Schemas.INT,"是否可以通过手机号码搜索到我","0：不可以、1：可以"));
		addField(new Field("webim",Schemas.INT,"是否开启webim","0：不开启、1：开启"));
	}
	@Override
	public String getName(){
		return "隐私信息";
	}
}
