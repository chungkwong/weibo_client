/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Statistics extends Subject{
	public static Statistics INSTANCE=new Statistics();
	private Statistics(){
		addField(new Field("status",Schemas.INT,"新微博未读数"));
		addField(new Field("follower",Schemas.INT,"新粉丝数"));
		addField(new Field("cmt",Schemas.INT,"新评论数"));
		addField(new Field("dm",Schemas.INT,"新私信数"));
		addField(new Field("mention_status",Schemas.INT,"新提及我的微博数"));
		addField(new Field("mention_cmt",Schemas.INT,"新提及我的评论数"));
		addField(new Field("group",Schemas.INT,"微群消息未读数"));
		addField(new Field("private_group",Schemas.INT,"私有微群消息未读数"));
		addField(new Field("notice",Schemas.INT,"新通知未读数"));
		addField(new Field("invite",Schemas.INT,"新邀请未读数"));
		addField(new Field("badge",Schemas.INT,"新勋章数"));
		addField(new Field("photo",Schemas.INT,"相册消息未读数"));
		addField(new Field("msgbox",Schemas.INT,"{{{3}}}"));
	}
	@Override
	public String getName(){
		return "消息未读数";
	}
}
