/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class ShortURL extends Subject{
	public static ShortURL INSTANCE=new ShortURL();
	private ShortURL(){
		addField(new Field("url_short",Schemas.STRING,"短链接"));
		addField(new Field("url_long",Schemas.STRING,"原始长链接",true));
		addField(new Field("type",Schemas.INT,"链接的类型","0：普通网页、1：视频、2：音乐、3：活动、5、投票"));
		addField(new Field("result",Schemas.BOOLEAN,"短链的是否可用"));
	}
	@Override
	public String getName(){
		return "短链";
	}
}
