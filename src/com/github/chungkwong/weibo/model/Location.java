/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Location extends Subject{
	public static Location INSTANCE=new Location();
	private Location(){
		addField(new Field("longitude",Schemas.STRING,"经度坐标"));
		addField(new Field("latitude",Schemas.STRING,"维度坐标"));
		addField(new Field("city",Schemas.STRING,"所在城市的城市代码"));
		addField(new Field("province",Schemas.STRING,"所在省份的省份代码"));
		addField(new Field("city_name",Schemas.STRING,"所在城市的城市名称",true));
		addField(new Field("province_name",Schemas.STRING,"所在省份的省份名称",true));
		addField(new Field("address",Schemas.STRING,"所在的实际地址"));
		addField(new Field("pinyin",Schemas.STRING,"地址的汉语拼音"));
		addField(new Field("more",Schemas.STRING,"更多信息"));
	}
	@Override
	public String getName(){
		return "位置";
	}
}
