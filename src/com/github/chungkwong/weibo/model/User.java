/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class User extends Subject{
	public static User INSTANCE=new User();
	private User(){
		addField(new Field("id",Schemas.LONG,"用户UID"));
		addField(new Field("idstr",Schemas.STRING,"字符串型的用户UID"));
		addField(new Field("screen_name",Schemas.STRING,"用户昵称",true));
		addField(new Field("name",Schemas.STRING,"友好显示名称"));
		addField(new Field("province",Schemas.INT,"用户所在省级ID"));
		addField(new Field("city",Schemas.INT,"用户所在城市ID"));
		addField(new Field("location",Schemas.STRING,"用户所在地"));
		addField(new Field("description",Schemas.STRING,"用户个人描述"));
		addField(new Field("url",Schemas.STRING,"用户博客地址"));
		addField(new Field("profile_image_url",Schemas.STRING,"用户头像地址（中图），50×50像素"));
		addField(new Field("profile_url",Schemas.STRING,"用户的微博统一URL地址"));
		addField(new Field("domain",Schemas.STRING,"用户的个性化域名"));
		addField(new Field("weihao",Schemas.STRING,"用户的微号"));
		addField(new Field("gender",Schemas.STRING,"性别","m：男、f：女、n：未知"));
		addField(new Field("followers_count",Schemas.INT,"粉丝数"));
		addField(new Field("friends_count",Schemas.INT,"关注数"));
		addField(new Field("statuses_count",Schemas.INT,"微博数"));
		addField(new Field("favourites_count",Schemas.INT,"收藏数"));
		addField(new Field("created_at",Schemas.STRING,"用户创建（注册）时间"));
		addField(new Field("following",Schemas.BOOLEAN,"暂未支持"));
		addField(new Field("allow_all_act_msg",Schemas.BOOLEAN,"是否允许所有人给我发私信"));
		addField(new Field("geo_enabled",Schemas.BOOLEAN,"是否允许标识用户的地理位置"));
		addField(new Field("verified",Schemas.BOOLEAN,"是否是微博认证用户，即加V用户"));
		addField(new Field("verified_type",Schemas.INT,"暂未支持"));
		addField(new Field("remark",Schemas.STRING,"用户备注信息"));
		addField(new Field("status",Status.INSTANCE,"用户的最近一条微博信息字段"));
		addField(new Field("allow_all_comment",Schemas.BOOLEAN,"是否允许所有人对我的微博进行评论"));
		addField(new Field("avatar_large",Schemas.STRING,"用户头像地址（大图），180×180像素"));
		addField(new Field("avatar_hd",Schemas.STRING,"用户头像地址（高清），高清头像原图"));
		addField(new Field("verified_reason",Schemas.STRING,"认证原因"));
		addField(new Field("follow_me",Schemas.BOOLEAN,"该用户是否关注当前登录用户"));
		addField(new Field("online_status",Schemas.INT,"用户的在线状态","0：不在线、1：在线"));
		addField(new Field("bi_followers_count",Schemas.INT,"用户的互粉数"));
		addField(new Field("lang",Schemas.STRING,"用户当前的语言版本","zh-cn：简体中文，zh-tw：繁体中文，en：英语"));
	}
	@Override
	public String getName(){
		return "用户";
	}
}
