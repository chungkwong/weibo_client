/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;
import com.github.chungkwong.json.*;
import javax.swing.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Schemas{
	public static final Schema BOOLEAN=new PrimitiveSchema();
	public static final Schema INT=new PrimitiveSchema();
	public static final Schema LONG=new PrimitiveSchema();
	public static final Schema STRING=new PrimitiveSchema();
	public static final Schema UNKNOWN=new UnknownSchema();
	public static final Schema toArraySchema(Schema schema){
		return (json)->{
			Box box=Box.createVerticalBox();
			((JSONArray)json).getElements().forEach((element)->box.add(schema.createViewer(element)));
			return box;
		};
	}
	public static final Schema toPackedSchema(String key,Schema schema){
		return new PackedSchema(key,schema);
	}
	static class PrimitiveSchema implements Schema{
		@Override
		public JComponent createViewer(JSONStuff json){
			JTextField field=new JTextField();
			field.setText(json.toString());
			field.setEditable(false);
			return field;
		}
	}
	static class UnknownSchema implements Schema{
		@Override
		public JComponent createViewer(JSONStuff json){
			JTextArea field=new JTextArea();
			field.setText(json.toString());
			field.setLineWrap(true);
			field.setEditable(false);
			return field;
		}
	}
	static class PackedSchema extends Subject{
		public PackedSchema(String key,Schema sc){
			addField(new Field(key,sc,"包装",true));
		}
		@Override
		public String getName(){
			return "包装";
		}
	}
}
