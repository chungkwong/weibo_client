/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.model;
import com.github.chungkwong.json.*;
import java.awt.*;
import java.util.*;
import javax.swing.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public abstract class Subject implements Schema{
	private final HashMap<String,Field> fields=new HashMap<>();
	public abstract String getName();
	protected void addField(Field f){
		fields.put(f.getIdentifier(),f);
	}
	public Field getField(String id){
		return fields.get(id);
	}
	@Override
	public JComponent createViewer(JSONStuff json){
		Box box=Box.createVerticalBox();
		box.add(createPanel((JSONObject)json,false));
		JButton expand=new JButton("详细");
		expand.addActionListener((e)->{
			JDialog dialog=new JDialog();
			dialog.setLayout(new BorderLayout());
			dialog.add(new JScrollPane(createPanel((JSONObject)json,true)));
			dialog.setSize(800,600);
			dialog.setVisible(true);
		});
		box.add(expand);
		return box;
	}
	public JComponent createPanel(JSONObject json,boolean detailed){
		JComponent panel = new JPanel();
		GroupLayout layout = new GroupLayout(panel);
		panel.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		GroupLayout.SequentialGroup hGroup=layout.createSequentialGroup();
		GroupLayout.SequentialGroup vGroup=layout.createSequentialGroup();
		GroupLayout.ParallelGroup col0=layout.createParallelGroup();
		GroupLayout.ParallelGroup col1=layout.createParallelGroup();
		json.getMembers().forEach((k,v)->{
			Field f=fields.get(((JSONString)k).getValue());
			JComponent key;
			JComponent val;
			if(f==null||!f.isGeneral()){
				if(detailed){
					key=Schemas.UNKNOWN.createViewer(k);
					val=Schemas.UNKNOWN.createViewer(v);
				}else
					return;
			}else{
				key=new JLabel(f.getDescription());
				val=f.getRange().createViewer(v);
				val.setToolTipText(f.getDetail());
			}
			col0.addComponent(key);
			col1.addComponent(val);
			vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(key).addComponent(val));
		});
		hGroup.addGroup(col0);
		hGroup.addGroup(col1);
		layout.setHorizontalGroup(hGroup);
		layout.setVerticalGroup(vGroup);
		return panel;
	}
}
