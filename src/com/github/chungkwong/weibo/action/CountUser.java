/*
 * Copyright (C) 2018 Chan Chung Kwong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.action;
import com.github.chungkwong.weibo.model.*;
/**
 *
 * @author Chan Chung Kwong
 */
public class CountUser implements WeiboAction{
	public static final CountUser INSTANCE=new CountUser();
	private static final Parameter[] parameters=new Parameter[]{
		new Parameter("access_token",true,Range.STRING,"采用OAuth授权方式为必填参数，OAuth授权后获得。"),
		new Parameter("uids",true,Range.STRING,"需要获取数据的用户UID，多个之间用逗号分隔，最多不超过100个。"),};
	private CountUser(){
	}
	@Override
	public String getName(){
		return "批量获取用户的粉丝数、关注数、微博数 ";
	}
	@Override
	public Parameter[] getParameters(){
		return parameters;
	}
	@Override
	public boolean isPost(){
		return false;
	}
	@Override
	public String getURL(){
		return "https://api.weibo.com/2/users/counts.json";
	}
	@Override
	public Schema getReturnSchema(){
		return Schemas.toArraySchema(Schemas.UNKNOWN);
	}
}
