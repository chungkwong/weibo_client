/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.action;
import com.github.chungkwong.weibo.model.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class HomeTimeline implements WeiboAction{
	public static final HomeTimeline INSTANCE=new HomeTimeline();
	private static final Parameter[] parameters=new Parameter[]{
		new Parameter("access_token",true,Range.STRING,"采用OAuth授权方式为必填参数，OAuth授权后获得。"),
		new Parameter("since_id",false,Range.INT,"若指定此参数，则返回ID比since_id大的微博（即比since_id时间晚的微博），默认为0。"),
		new Parameter("max_id",false,Range.INT,"若指定此参数，则返回ID小于或等于max_id的微博，默认为0。"),
		new Parameter("count",false,Range.INT,"单页返回的记录条数，最大不超过100，默认为20。"),
		new Parameter("page",false,Range.INT,"返回结果的页码，默认为1。"),
		new Parameter("base_app",false,Range.INT,"是否只获取当前应用的数据。0为否（所有数据），1为是（仅当前应用），默认为0。"),
		new Parameter("feature",false,Range.INT,"过滤类型ID，0：全部、1：原创、2：图片、3：视频、4：音乐，默认为0"),
		new Parameter("trim_user",false,Range.INT,"返回值中user字段开关，0：返回完整user字段、1：user字段仅返回user_id，默认为0。"),
	};
	private HomeTimeline(){

	}
	@Override
	public String getName(){
		return "获取当前登录用户及其所关注（授权）用户的最新微博";
	}
	@Override
	public Parameter[] getParameters(){
		return parameters;
	}
	@Override
	public boolean isPost(){
		return false;
	}
	@Override
	public String getURL(){
		return "https://api.weibo.com/2/statuses/home_timeline.json";
	}
	@Override
	public Schema getReturnSchema(){
		return Schemas.toPackedSchema("statuses",Schemas.toArraySchema(Status.INSTANCE));
	}
}