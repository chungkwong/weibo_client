/*
 * Copyright (C) 2018 Chan Chung Kwong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.action;
import com.github.chungkwong.weibo.model.*;
/**
 *
 * @author Chan Chung Kwong
 */
public class Friend implements WeiboAction{
	public static final Friend INSTANCE=new Friend();
	private static final Parameter[] parameters=new Parameter[]{
		new Parameter("access_token",true,Range.STRING,"采用OAuth授权方式为必填参数，OAuth授权后获得。"),
		new Parameter("uid",false,Range.INT,"需要查询的用户UID。"),
		new Parameter("screen_name",false,Range.STRING,"需要查询的用户昵称。"),
		new Parameter("count",false,Range.INT,"单页返回的记录条数，默认为5，最大不超过5。"),
		new Parameter("cursor",false,Range.INT,"返回结果的游标，下一页用返回值里的next_cursor，上一页用previous_cursor，默认为0。"),
		new Parameter("trim_status",false,Range.INT,"返回值中user字段中的status字段开关，0：返回完整status字段、1：status字段仅返回status_id，默认为1。"),};
	private Friend(){
	}
	@Override
	public String getName(){
		return "获取用户的关注列表";
	}
	@Override
	public Parameter[] getParameters(){
		return parameters;
	}
	@Override
	public boolean isPost(){
		return false;
	}
	@Override
	public String getURL(){
		return "https://api.weibo.com/2/friendships/friends.json";
	}
	@Override
	public Schema getReturnSchema(){
		return Schemas.toPackedSchema("users",Schemas.toArraySchema(User.INSTANCE));
	}
}
