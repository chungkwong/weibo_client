/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.action;
import com.github.chungkwong.weibo.model.*;
/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Share implements WeiboAction{
	public static final Share INSTANCE=new Share();
	private static final Parameter[] parameters=new Parameter[]{
		new Parameter("access_token",true,Range.STRING,"采用OAuth授权方式为必填参数，OAuth授权后获得。"),
		new Parameter("status",true,Range.STRING,"要发布的微博文本内容，内容不超过140个汉字。"),
		new Parameter("pic",false,Range.BINARY,"要上传的图片，仅支持JPEG、GIF、PNG格式，图片大小小于5M。"),
		new Parameter("rip",false,Range.STRING,"开发者上报的操作用户真实IP，形如：211.156.0.1。")
	};
	private Share(){
	}
	@Override
	public String getName(){
		return "第三方分享一条链接到微博";
	}
	@Override
	public Parameter[] getParameters(){
		return parameters;
	}
	@Override
	public boolean isPost(){
		return true;
	}
	@Override
	public String getURL(){
		return "https://api.weibo.com/2/statuses/share.json";
	}
	@Override
	public boolean isMultiPart(){
		return true;
	}
	@Override
	public Schema getReturnSchema(){
		return Status.INSTANCE;
	}
}
