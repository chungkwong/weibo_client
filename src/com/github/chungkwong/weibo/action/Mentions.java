/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.weibo.action;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class Mentions implements WeiboAction{
	public static final Mentions INSTANCE=new Mentions();
	private static final Parameter[] parameters=new Parameter[]{
		new Parameter("access_token",true,Range.STRING,"采用OAuth授权方式为必填参数，OAuth授权后获得。"),
		new Parameter("since_id",false,Range.INT,"若指定此参数，则返回ID比since_id大的微博（即比since_id时间晚的微博），默认为0。"),
		new Parameter("max_id",false,Range.INT,"若指定此参数，则返回ID小于或等于max_id的微博，默认为0。"),
		new Parameter("count",false,Range.INT,"单页返回的记录条数，最大不超过200，默认为20。"),
		new Parameter("page",false,Range.INT,"返回结果的页码，默认为1。"),
		new Parameter("filter_by_author",false,Range.INT,"作者筛选类型，0：全部、1：我关注的人、2：陌生人，默认为0。"),
		new Parameter("filter_by_source",false,Range.INT,"来源筛选类型，0：全部、1：来自微博、2：来自微群，默认为0。"),
		new Parameter("filter_by_type",false,Range.INT,"原创筛选类型，0：全部微博、1：原创的微博，默认为0。"),
	};
	private Mentions(){

	}
	@Override
	public String getName(){
		return "获取最新的提到登录用户的微博列表";
	}
	@Override
	public Parameter[] getParameters(){
		return parameters;
	}
	@Override
	public boolean isPost(){
		return false;
	}
	@Override
	public String getURL(){
		return "https://api.weibo.com/2/statuses/mentions.json";
	}
}