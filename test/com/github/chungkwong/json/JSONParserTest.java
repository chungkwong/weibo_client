/*
 * Copyright (C) 2016 Chan Chung Kwong <1m02math@126.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.github.chungkwong.json;

import java.io.*;
import java.util.*;
import org.junit.*;

/**
 *
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public class JSONParserTest{
	public JSONParserTest(){
	}
	@Test
	public void testValue(){
		assertParseTo("null",JSONNull.INSTANCE);
		assertParseTo("true",JSONBoolean.TRUE);
		assertParseTo("false",JSONBoolean.FALSE);
		assertException("no");
		assertException("tok");
		assertException("java");
	}
	@Test
	public void testObject(){
		assertParseTo("{}",new JSONObject(Collections.EMPTY_MAP));
		assertParseTo("{\"hello\":-4.52e+4}",new JSONObject(buildMap(new JSONString("hello"),new JSONNumber(-4.52e+4))));
		assertParseTo("{\"hello\":null,\"my\":{}}",new JSONObject(buildMap(new JSONString("hello"),JSONNull.INSTANCE,new JSONString("my"),new JSONObject(Collections.EMPTY_MAP))));
	}
	@Test
	public void testArray(){
		assertParseTo("[]",new JSONArray(Collections.EMPTY_LIST));
		assertParseTo("[true]",new JSONArray(Arrays.asList(JSONBoolean.TRUE)));
		assertParseTo("[\"hello\",-4.52e+4,null]",new JSONArray(Arrays.asList(new JSONString("hello"),new JSONNumber(-4.52e+4),JSONNull.INSTANCE)));
		assertParseTo("[[]]",new JSONArray(Arrays.asList(new JSONArray(Arrays.asList()))));
	}
	@Test
	public void testString(){
		assertParseTo("\"\"",new JSONString(""));
		assertParseTo("\"ab 4%\"",new JSONString("ab 4%"));
		assertParseTo("\"\\\"\\\\\\/\\b\\f\\n\\r\\t\\u0344\"",new JSONString("\"\\/\b\f\n\r\t\u0344"));
		assertException("\"");
		assertException("\"\\a\"");
		assertException("\"\\u123\"");
	}
	@Test
	public void testNumber(){
		assertParseTo("0",new JSONNumber(0));
		assertParseTo("-24",new JSONNumber(-24));
		assertParseTo("-2.",new JSONNumber(-2.));
		assertParseTo("-2.56",new JSONNumber(-2.56));
		assertParseTo("4e7",new JSONNumber(4e7));
		assertParseTo("4E7",new JSONNumber(4e7));
		assertParseTo("4.1e+7",new JSONNumber(4.1e7));
		assertParseTo("4E+7",new JSONNumber(4e7));
		assertParseTo("4e-10",new JSONNumber(4e-10));
		assertParseTo("-4e-7",new JSONNumber(-4e-7));
	}
	public void assertParseTo(String code,JSONStuff obj){
		try{
			Assert.assertEquals(JSONParser.parse(code),obj);
		}catch(IOException|SyntaxException ex){
			Assert.assertTrue(false);
		}
	}
	public void assertException(String code){
		try{
			JSONParser.parse(code);
			Assert.assertTrue(false);
		}catch(Exception ex){

		}
	}
	public static <T> Map<T,T> buildMap(T... col){
		HashMap<T,T> map=new HashMap<>();
		for(int i=0;i<col.length;i+=2)
			map.put(col[i],col[i+1]);
		return map;
	}
}
